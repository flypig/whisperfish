<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name></name>
    <message id="whisperfish-cover-new-label">
        <location filename="../qml/cover/UnreadLabel.qml" line="25"/>
        <source>New</source>
        <extracomment>Cover new message label</extracomment>
        <translation>Nowe</translation>
    </message>
    <message id="whisperfish-session-has-attachment">
        <location filename="../qml/delegates/SessionDelegate.qml" line="33"/>
        <source>Attachment</source>
        <extracomment>Session contains an attachment label</extracomment>
        <translation>Załącznik</translation>
    </message>
    <message id="whisperfish-session-delete-all">
        <location filename="../qml/delegates/SessionDelegate.qml" line="48"/>
        <source>Deleting all messages</source>
        <extracomment>Delete all messages from session</extracomment>
        <translation>Usuwanie wszystkich wiadomości</translation>
    </message>
    <message id="whisperfish-session-note-to-self">
        <location filename="../qml/delegates/SessionDelegate.qml" line="121"/>
        <source>Note to self</source>
        <extracomment>Name of the conversation with one&apos;s own number</extracomment>
        <translation>Uwaga do siebie</translation>
    </message>
    <message id="whisperfish-message-preview-draft">
        <location filename="../qml/delegates/SessionDelegate.qml" line="140"/>
        <source>Draft: %1</source>
        <extracomment>Message preview for a saved, unsent message</extracomment>
        <translation>Szkic: %1</translation>
    </message>
    <message id="whisperfish-session-delete">
        <location filename="../qml/delegates/SessionDelegate.qml" line="259"/>
        <source>Delete conversation</source>
        <extracomment>Delete all messages from session menu</extracomment>
        <translation>Usuń konwersację</translation>
    </message>
    <message id="whisperfish-delete-session">
        <source>Delete Conversation</source>
        <extracomment>Delete all messages from session menu</extracomment>
        <translation type="vanished">Usuń Konwersację</translation>
    </message>
    <message id="whisperfish-notification-default-message">
        <location filename="../qml/harbour-whisperfish.qml" line="67"/>
        <source>New Message</source>
        <extracomment>Default label for new message notification</extracomment>
        <translation>Nowa Wiadomość</translation>
    </message>
    <message id="whisperfish-fatal-error-setup-client">
        <location filename="../qml/harbour-whisperfish.qml" line="142"/>
        <source>Failed to setup Signal client</source>
        <extracomment>Failed to setup signal client error message</extracomment>
        <translation>Nie udało się skonfigurować klienta Signal</translation>
    </message>
    <message id="whisperfish-fatal-error-invalid-datastore">
        <location filename="../qml/harbour-whisperfish.qml" line="147"/>
        <source>Failed to setup data storage</source>
        <oldsource>Failed to setup datastore</oldsource>
        <extracomment>Failed to setup datastore error message</extracomment>
        <translation>Nie udało się skonfigurować magazynu danych</translation>
    </message>
    <message id="whisperfish-session-section-today">
        <location filename="../qml/pages/MainPage.qml" line="111"/>
        <source>Today</source>
        <extracomment>Session section label for today</extracomment>
        <translation>Dzisiaj</translation>
    </message>
    <message id="whisperfish-session-section-yesterday">
        <location filename="../qml/pages/MainPage.qml" line="116"/>
        <source>Yesterday</source>
        <extracomment>Session section label for yesterday</extracomment>
        <translation>Wczoraj</translation>
    </message>
    <message id="whisperfish-session-section-older">
        <location filename="../qml/pages/MainPage.qml" line="121"/>
        <source>Older</source>
        <extracomment>Session section label for older</extracomment>
        <translation>Starsze</translation>
    </message>
    <message id="whisperfish-about">
        <location filename="../qml/pages/About.qml" line="20"/>
        <source>About Whisperfish</source>
        <extracomment>Title for about page</extracomment>
        <translation>O Whisperfish</translation>
    </message>
    <message id="whisperfish-version">
        <location filename="../qml/pages/About.qml" line="33"/>
        <source>Whisperfish v%1</source>
        <extracomment>Whisperfish version string</extracomment>
        <translation>Whisperfish v%1</translation>
    </message>
    <message id="whisperfish-description">
        <location filename="../qml/pages/About.qml" line="43"/>
        <source>Signal client for Sailfish OS</source>
        <extracomment>Whisperfish description</extracomment>
        <translation>Klient Signal dla Sailfish OS</translation>
    </message>
    <message id="whisperfish-build-id">
        <location filename="../qml/pages/About.qml" line="54"/>
        <source>Build ID: %1</source>
        <extracomment>Whisperfish long version string and build ID</extracomment>
        <translation>ID kompilacji: %1</translation>
    </message>
    <message id="whisperfish-copyright">
        <location filename="../qml/pages/About.qml" line="64"/>
        <source>Copyright</source>
        <extracomment>Copyright</extracomment>
        <translation>Prawa autorskie</translation>
    </message>
    <message id="whisperfish-liberapay">
        <location filename="../qml/pages/About.qml" line="87"/>
        <source>Support on Liberapay</source>
        <extracomment>Support on Liberapay</extracomment>
        <translation>Wsparcie na Liberapay</translation>
    </message>
    <message id="whisperfish-source-code">
        <location filename="../qml/pages/About.qml" line="97"/>
        <source>Source Code</source>
        <extracomment>Source Code</extracomment>
        <translation>Kod Źródłowy</translation>
    </message>
    <message id="whisperfish-bug-report">
        <location filename="../qml/pages/About.qml" line="107"/>
        <source>Report a Bug</source>
        <extracomment>Report a Bug</extracomment>
        <translation>Zgłoś Błąd</translation>
    </message>
    <message id="whisperfish-about-wiki-link">
        <location filename="../qml/pages/About.qml" line="117"/>
        <source>Visit the Wiki</source>
        <extracomment>Visit the Wiki button, tapping links to the Whisperfish Wiki</extracomment>
        <translation>Odwiedź Wiki</translation>
    </message>
    <message id="whisperfish-extra-copyright">
        <location filename="../qml/pages/About.qml" line="126"/>
        <source>Additional Copyright</source>
        <extracomment>Additional Copyright</extracomment>
        <translation>Dodatkowe Prawa Autorskie</translation>
    </message>
    <message id="whisperfish-add-confirm">
        <location filename="../qml/pages/AddDevice.qml" line="25"/>
        <source>Add</source>
        <extracomment>&quot;Add&quot; message, shown in the link device dialog</extracomment>
        <translation>Dodaj</translation>
    </message>
    <message id="whisperfish-add-device">
        <location filename="../qml/pages/AddDevice.qml" line="33"/>
        <source>Add Device</source>
        <extracomment>Add Device, shown as pull-down menu item</extracomment>
        <translation>Dodaj Urządzenie</translation>
    </message>
    <message id="whisperfish-device-url">
        <location filename="../qml/pages/AddDevice.qml" line="43"/>
        <source>Device URL</source>
        <extracomment>Device URL, text input for pasting the QR-scanned code</extracomment>
        <translation>URL urządzenia</translation>
    </message>
    <message id="whisperfish-device-link-instructions">
        <location filename="../qml/pages/AddDevice.qml" line="54"/>
        <source>Install Signal Desktop. Use the CodeReader application to scan the QR code displayed on Signal Desktop and copy and paste the URL here.</source>
        <extracomment>Instructions on how to scan QR code for device linking</extracomment>
        <translation>Zainstaluj Signal Desktop. Użyj aplikacji CodeReader, aby zeskanować kod QR wyświetlony w Signal Desktop, skopiuj i wklej URL tutaj.</translation>
    </message>
    <message id="whisperfish-attachment-from-self">
        <source>Me</source>
        <extracomment>Personalized placeholder showing the attachment is from oneself</extracomment>
        <translation type="vanished">Ja</translation>
    </message>
    <message id="whisperfish-attachment-from-contact">
        <source>From %1</source>
        <extracomment>Personalized placeholder showing the attachment is from contact</extracomment>
        <translation type="vanished">Od %1</translation>
    </message>
    <message id="whisperfish-chatinput-contact">
        <source>Hi %1</source>
        <extracomment>Personalized placeholder for chat input, e.g. &quot;Hi John&quot;</extracomment>
        <translation type="vanished">Cześć, %1</translation>
    </message>
    <message id="whisperfish-chatinput-generic">
        <source>Hi</source>
        <extracomment>Generic placeholder for chat input</extracomment>
        <translation type="vanished">Cześć</translation>
    </message>
    <message id="whisperfish-select-file">
        <source>Select file</source>
        <extracomment>Title for file picker page</extracomment>
        <translation type="vanished">Wybierz plik</translation>
    </message>
    <message id="whisperfish-choose-country-code">
        <source>Choose Country Code</source>
        <extracomment>Directions for choosing country code</extracomment>
        <translation type="vanished">Wybierz Kod Kraju</translation>
    </message>
    <message id="whisperfish-select-picture">
        <location filename="../qml/pages/ImagePicker.qml" line="44"/>
        <source>Select picture</source>
        <extracomment>Title for image picker page</extracomment>
        <translation>Wybierz obrazek</translation>
    </message>
    <message id="whisperfish-add-linked-device">
        <location filename="../qml/pages/LinkedDevices.qml" line="17"/>
        <source>Add</source>
        <extracomment>Menu option to add new linked device</extracomment>
        <translation>Dodaj</translation>
    </message>
    <message id="whisperfish-refresh-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="30"/>
        <source>Refresh</source>
        <extracomment>Menu option to refresh linked devices</extracomment>
        <translation>Odśwież</translation>
    </message>
    <message id="whisperfish-linked-devices">
        <location filename="../qml/pages/LinkedDevices.qml" line="39"/>
        <source>Linked Devices</source>
        <extracomment>Title for Linked Devices page</extracomment>
        <translation>Połączone Urządzenia</translation>
    </message>
    <message id="whisperfish-device-unlink-message">
        <location filename="../qml/pages/LinkedDevices.qml" line="49"/>
        <source>Unlinking</source>
        <extracomment>Unlinking remorse info message for unlinking secondary devices.</extracomment>
        <translation>Odłączanie</translation>
    </message>
    <message id="whisperfish-current-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="65"/>
        <source>Current device (Whisperfish, %1)</source>
        <extracomment>Linked device title for current Whisperfish</extracomment>
        <translation>Bieżące urządzenie (Whisperfish, %1)</translation>
    </message>
    <message id="whisperfish-device-name">
        <location filename="../qml/pages/LinkedDevices.qml" line="69"/>
        <source>Device %1</source>
        <extracomment>Linked device name</extracomment>
        <translation>Urządzenie %1</translation>
    </message>
    <message id="whisperfish-device-link-date">
        <location filename="../qml/pages/LinkedDevices.qml" line="81"/>
        <source>Linked: %1</source>
        <extracomment>Linked device date</extracomment>
        <translation>Połączone: %1</translation>
    </message>
    <message id="whisperfish-device-last-active">
        <location filename="../qml/pages/LinkedDevices.qml" line="98"/>
        <source>Last active: %1</source>
        <extracomment>Linked device last active date</extracomment>
        <translation>Ostatnio aktywne: %1</translation>
    </message>
    <message id="whisperfish-device-unlink">
        <location filename="../qml/pages/LinkedDevices.qml" line="118"/>
        <source>Unlink</source>
        <extracomment>Device unlink menu option</extracomment>
        <translation>Odłącz</translation>
    </message>
    <message id="whisperfish-no-messages-hint-text">
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Pull down to start a new conversation.</source>
        <extracomment>No messages found, hint on what to do</extracomment>
        <translation>Przeciągnij w dół, aby rozpocząć nową rozmowę.</translation>
    </message>
    <message id="whisperfish-registration-complete">
        <source>Registration complete!</source>
        <extracomment>Registration complete remorse message</extracomment>
        <translation type="vanished">Rejestracja zakończona!</translation>
    </message>
    <message id="whisperfish-error-invalid-datastore">
        <source>ERROR - Failed to setup datastore</source>
        <extracomment>Failed to setup datastore error message</extracomment>
        <translation type="vanished">BŁĄD - Ustawienie datastore nie powiodło się</translation>
    </message>
    <message id="whisperfish-error-invalid-number">
        <source>ERROR - Invalid phone number registered with Signal</source>
        <extracomment>Invalid phone number error message</extracomment>
        <translation type="vanished">BŁĄD - Niewłaściwy numer telefonu zarejestrowany w Signal</translation>
    </message>
    <message id="whisperfish-error-setup-client">
        <source>ERROR - Failed to setup Signal client</source>
        <extracomment>Failed to setup signal client error message</extracomment>
        <translation type="vanished">BŁĄD - Ustawienie klienta Signal nie powiodło się</translation>
    </message>
    <message id="whisperfish-about-menu">
        <location filename="../qml/pages/MainPage.qml" line="25"/>
        <source>About Whisperfish</source>
        <extracomment>About whisperfish menu item</extracomment>
        <translation>O Whisperfish</translation>
    </message>
    <message id="whisperfish-settings-menu">
        <location filename="../qml/pages/MainPage.qml" line="31"/>
        <source>Settings</source>
        <extracomment>Whisperfish settings menu item</extracomment>
        <translation>Ustawienia</translation>
    </message>
    <message id="whisperfish-new-group-menu">
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>New Group</source>
        <extracomment>Whisperfish new group menu item</extracomment>
        <translation>Nowa Grupa</translation>
    </message>
    <message id="whisperfish-new-message-menu">
        <location filename="../qml/pages/MainPage.qml" line="62"/>
        <source>New Message</source>
        <extracomment>Whisperfish new message menu item</extracomment>
        <translation>Nowa Wiadomość</translation>
    </message>
    <message id="whisperfish-no-messages-found">
        <source>No messages</source>
        <extracomment>Whisperfish no messages found message</extracomment>
        <translation type="vanished">Brak wiadomości</translation>
    </message>
    <message id="whisperfish-registration-required-message">
        <location filename="../qml/pages/MainPage.qml" line="89"/>
        <source>Registration required</source>
        <extracomment>Whisperfish registration required message</extracomment>
        <translation>Wymagana rejestracja</translation>
    </message>
    <message id="whisperfish-locked-message">
        <location filename="../qml/pages/MainPage.qml" line="93"/>
        <source>Locked</source>
        <extracomment>Whisperfish locked message</extracomment>
        <translation>Zablokowane</translation>
    </message>
    <message id="whisperfish-group-label">
        <source>Group: %1</source>
        <extracomment>Group message label</extracomment>
        <translation type="vanished">Grupa: %1</translation>
    </message>
    <message id="whisperfish-delete-message">
        <location filename="../qml/components/MessagesView.qml" line="195"/>
        <source>Deleting</source>
        <extracomment>Deleting message remorse</extracomment>
        <translation>Usuwanie</translation>
    </message>
    <message id="whisperfish-resend-message">
        <location filename="../qml/components/MessagesView.qml" line="205"/>
        <source>Resending</source>
        <extracomment>Resend message remorse</extracomment>
        <translation>Ponowne wysyłanie</translation>
    </message>
    <message id="whisperfish-copy-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="237"/>
        <source>Copy</source>
        <extracomment>Copy message menu item</extracomment>
        <translation>Kopiuj</translation>
    </message>
    <message id="whisperfish-open-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="244"/>
        <source>Open</source>
        <extracomment>Open attachment message menu item</extracomment>
        <translation>Otwórz</translation>
    </message>
    <message id="whisperfish-delete-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="251"/>
        <source>Delete</source>
        <extracomment>Delete message menu item</extracomment>
        <translation>Usuń</translation>
    </message>
    <message id="whisperfish-resend-message-menu">
        <location filename="../qml/components/MessagesView.qml" line="258"/>
        <source>Resend</source>
        <extracomment>Resend message menu item</extracomment>
        <translation>Wyślij ponownie</translation>
    </message>
    <message id="whisperfish-reset-session-menu">
        <location filename="../qml/pages/VerifyIdentity.qml" line="18"/>
        <source>Reset Secure Session</source>
        <extracomment>Reset secure session menu item</extracomment>
        <translation>Resetuj bezpieczną sesję</translation>
    </message>
    <message id="whisperfish-reset-session-message">
        <location filename="../qml/pages/VerifyIdentity.qml" line="23"/>
        <source>Resetting secure session</source>
        <extracomment>Reset secure session remorse message</extracomment>
        <translation>Resetowanie bezpiecznej sesji</translation>
    </message>
    <message id="whisperfish-numeric-fingerprint-directions">
        <location filename="../qml/pages/VerifyIdentity.qml" line="63"/>
        <source>If you wish to verify the security of your end-to-end encryption with %1, compare the numbers above with the numbers on their device.</source>
        <extracomment>Numeric fingerprint instructions</extracomment>
        <translation>Celem weryfikacji bezpieczeństwa szyfrowania końcowego z %1, porównaj numery powyżej z numerami na urządzeniu odbiorcy.</translation>
    </message>
    <message id="whisperfish-recipient-number-invalid-chars">
        <location filename="../qml/pages/NewMessage.qml" line="59"/>
        <source>This phone number contains invalid characters.</source>
        <extracomment>invalid recipient phone number: invalid characters</extracomment>
        <translation>Ten numer telefonu zawiera nieprawidłowe znaki.</translation>
    </message>
    <message id="whisperfish-recipient-local-number-not-allowed">
        <location filename="../qml/pages/NewMessage.qml" line="64"/>
        <source>Please set a country code in the settings, or use the international format.</source>
        <extracomment>invalid recipient phone number: local numbers are not allowed</extracomment>
        <translation>Ustaw kod kraju w ustawieniach lub użyj formatu międzynarodowego.</translation>
    </message>
    <message id="whisperfish-recipient-number-invalid-unspecified">
        <location filename="../qml/pages/NewMessage.qml" line="68"/>
        <source>This phone number appears to be invalid.</source>
        <extracomment>invalid recipient phone number: failed to format</extracomment>
        <translation>Ten numer telefonu wydaje się być nieprawidłowy.</translation>
    </message>
    <message id="whisperfish-new-message-title">
        <location filename="../qml/pages/NewMessage.qml" line="96"/>
        <source>New message</source>
        <extracomment>New message page title</extracomment>
        <translation>Nowa wiadomość</translation>
    </message>
    <message id="whisperfish-group-name-label">
        <location filename="../qml/pages/NewGroup.qml" line="47"/>
        <source>Group Name</source>
        <extracomment>Group name label</extracomment>
        <translation>Nazwa Grupy</translation>
    </message>
    <message id="whisperfish-group-name-placeholder">
        <location filename="../qml/pages/NewGroup.qml" line="50"/>
        <source>Group Name</source>
        <extracomment>Group name placeholder</extracomment>
        <translation>Nazwa Grupy</translation>
    </message>
    <message id="whisperfish-new-group-title">
        <location filename="../qml/pages/NewGroup.qml" line="38"/>
        <source>New Group</source>
        <extracomment>New group page title</extracomment>
        <translation>Nowa Grupa</translation>
    </message>
    <message id="whisperfish-new-message-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="114"/>
        <location filename="../qml/pages/NewMessage.qml" line="118"/>
        <source>Recipient</source>
        <extracomment>New message recipient label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Odbiorca</translation>
    </message>
    <message id="whisperfish-new-group-message-members">
        <location filename="../qml/pages/NewGroup.qml" line="69"/>
        <location filename="../qml/pages/NewGroup.qml" line="73"/>
        <source>Members</source>
        <extracomment>New group message members label
----------
Summary of all selected recipients, e.g. &quot;Bob, Jane, 75553243&quot;</extracomment>
        <translation>Członkowie</translation>
    </message>
    <message id="whisperfish-error-invalid-group-name">
        <location filename="../qml/pages/NewGroup.qml" line="110"/>
        <source>Please name the group</source>
        <extracomment>Invalid group name error</extracomment>
        <translation>Nazwij grupę</translation>
    </message>
    <message id="whisperfish-error-invalid-group-members">
        <location filename="../qml/pages/NewGroup.qml" line="106"/>
        <source>Please select group members</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Wybierz członków grupy</translation>
    </message>
    <message id="whisperfish-error-invalid-recipient">
        <location filename="../qml/pages/NewMessage.qml" line="174"/>
        <source>Invalid recipient</source>
        <extracomment>Invalid recipient error</extracomment>
        <translation>Niewłaściwy odbiorca</translation>
    </message>
    <message id="whisperfish-enter-password">
        <source>Enter your password</source>
        <extracomment>Enter password prompt</extracomment>
        <translation type="vanished">Wprowadź swoje hasło</translation>
    </message>
    <message id="whisperfish-set-password">
        <source>Set your password</source>
        <extracomment>Set password prompt</extracomment>
        <translation type="vanished">Ustaw swoje hasło</translation>
    </message>
    <message id="whisperfish-initial-setup-welcome-title">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="16"/>
        <source>Welcome to Whisperfish</source>
        <extracomment>welcome screen title when creating a new database</extracomment>
        <translation>Witaj w Whisperfish</translation>
    </message>
    <message id="whisperfish-setup-password-prompt">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="20"/>
        <source>Set a new password to secure your conversations.</source>
        <extracomment>new password setup prompt</extracomment>
        <translation>Ustaw nowe hasło, aby zabezpieczyć swoje rozmowy.</translation>
    </message>
    <message id="whisperfish-password-label-too-short">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="71"/>
        <location filename="../qml/pages/SetupPasswordPage.qml" line="100"/>
        <source>Password is too short</source>
        <extracomment>Password label when too short</extracomment>
        <translation>Hasło jest za krótkie</translation>
    </message>
    <message id="whisperfish-password-label">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="74"/>
        <location filename="../qml/pages/UnlockPage.qml" line="73"/>
        <source>Password</source>
        <extracomment>Password label</extracomment>
        <translation>Hasło</translation>
    </message>
    <message id="whisperfish-new-password-placeholder">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="77"/>
        <source>Your new password</source>
        <extracomment>New password input placeholder</extracomment>
        <translation>Twoje nowe hasło</translation>
    </message>
    <message id="whisperfish-password-repeated-label">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="95"/>
        <source>Repeat the password</source>
        <oldsource>Repeated password</oldsource>
        <extracomment>repeated password input label</extracomment>
        <translation>Powtórz hasło</translation>
    </message>
    <message id="whisperfish-password-repeated-label-wrong">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="103"/>
        <source>Passwords do not match</source>
        <extracomment>repeated password input label if passwords don&apos;t match</extracomment>
        <translation>Hasła nie pasują do siebie</translation>
    </message>
    <message id="whisperfish-new-password-repeat-placeholder">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="107"/>
        <source>Repeat your new password</source>
        <extracomment>Repeated new password input placeholder</extracomment>
        <translation>Powtórz swoje nowe hasło</translation>
    </message>
    <message id="whisperfish-skip-button-label">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="131"/>
        <source>Skip</source>
        <extracomment>skip button label</extracomment>
        <translation>Pomiń</translation>
    </message>
    <message id="whisperfish-unlock-page-title">
        <location filename="../qml/pages/UnlockPage.qml" line="9"/>
        <source>Unlock</source>
        <extracomment>unlock page title</extracomment>
        <translation>Odblokuj</translation>
    </message>
    <message id="whisperfish-unlock-welcome-title">
        <location filename="../qml/pages/UnlockPage.qml" line="12"/>
        <source>Whisperfish</source>
        <extracomment>unlock page welcome title, centered on screen</extracomment>
        <translation>Whisperfish</translation>
    </message>
    <message id="whisperfish-unlock-password-prompt">
        <location filename="../qml/pages/UnlockPage.qml" line="15"/>
        <source>Please enter your password to unlock your conversations.</source>
        <extracomment>unlock page password prompt</extracomment>
        <translation>Wprowadź hasło, aby odblokować rozmowy.</translation>
    </message>
    <message id="whisperfish-fatal-error-msg-not-registered">
        <location filename="../qml/pages/UnlockPage.qml" line="27"/>
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="67"/>
        <source>You are not registered.</source>
        <extracomment>fatal error when trying to unlock the db when not registered</extracomment>
        <translation>Nie jesteś zarejestrowany.</translation>
    </message>
    <message id="whisperfish-unlock-try-again">
        <location filename="../qml/pages/UnlockPage.qml" line="52"/>
        <source>Please try again</source>
        <extracomment>input field placeholder after failed attempt to unlock (keep it short)</extracomment>
        <translation>Proszę spróbuj ponownie</translation>
    </message>
    <message id="whisperfish-password-placeholder">
        <location filename="../qml/pages/UnlockPage.qml" line="76"/>
        <source>Your password</source>
        <oldsource>Password</oldsource>
        <extracomment>password placeholder</extracomment>
        <translation>Hasło</translation>
    </message>
    <message id="whisperfish-unlock-button-label">
        <location filename="../qml/pages/UnlockPage.qml" line="85"/>
        <source>Unlock</source>
        <extracomment>unlock button label</extracomment>
        <translation>Odblokuj</translation>
    </message>
    <message id="whisperfish-verify-password-label">
        <source>Verify Password</source>
        <extracomment>Verify Password label</extracomment>
        <translation type="vanished">Zweryfikuj Hasło</translation>
    </message>
    <message id="whisperfish-verify-password-placeholder">
        <source>Verify Password</source>
        <extracomment>Verify Password label</extracomment>
        <translation type="vanished">Zweryfikuj Hasło</translation>
    </message>
    <message id="whisperfish-password-info">
        <location filename="../qml/pages/SetupPasswordPage.qml" line="24"/>
        <source>Whisperfish stores identity keys, session state, and local message data encrypted on disk. The password you set is not stored anywhere and you will not be able to restore your data if you lose your password. Note: Attachments are currently stored unencrypted. You can disable storing of attachments in the Settings page.</source>
        <extracomment>Whisperfish password informational message</extracomment>
        <translation>Whisperfish przechowuje klucze identyfikacyjne, stan sesji oraz dane wiadomości w formie zaszyfrowanej na dysku. Ustawione hasło nie jest nigdzie przechowywane i nie będzie możliwości odzyskania danych w przypadku utraty hasła. Uwaga: Załączniki są na chwilę obecną przechowywane w formie nieszyfrowanej. Przechowywanie załączników można wyłączyć na stronie Ustawień.</translation>
    </message>
    <message id="whisperfish-register-accept">
        <source>Register</source>
        <extracomment>Register accept text</extracomment>
        <translation type="vanished">Zarejestruj</translation>
    </message>
    <message id="whisperfish-registration-message">
        <location filename="../qml/pages/RegisterPage.qml" line="16"/>
        <source>Enter the phone number you want to register with Signal.</source>
        <oldsource>Connect with Signal</oldsource>
        <extracomment>registration prompt text</extracomment>
        <translation>Wprowadź numer telefonu, który chcesz zarejestrować w Signal.</translation>
    </message>
    <message id="whisperfish-phone-number-input-label">
        <source>International phone number</source>
        <oldsource>Phone number (E.164 format)</oldsource>
        <extracomment>Phone number input</extracomment>
        <translation type="obsolete">Numer telefonu (format E.164)</translation>
    </message>
    <message id="whisperfish-phone-number-input-placeholder">
        <source>+18875550100</source>
        <oldsource>18875550100</oldsource>
        <extracomment>Phone number placeholder</extracomment>
        <translation type="obsolete">18875550100</translation>
    </message>
    <message id="whisperfish-share-contacts-label">
        <location filename="../qml/pages/RegisterPage.qml" line="160"/>
        <location filename="../qml/pages/Settings.qml" line="198"/>
        <source>Share Contacts</source>
        <extracomment>Share contacts label
----------
Settings page share contacts</extracomment>
        <translation>Podziel się Kontaktami</translation>
    </message>
    <message id="whisperfish-share-contacts-description">
        <location filename="../qml/pages/RegisterPage.qml" line="163"/>
        <location filename="../qml/pages/Settings.qml" line="201"/>
        <source>Allow Signal to use your local contact list, to find other Signal users.</source>
        <extracomment>Share contacts description</extracomment>
        <translation>Pozwól Signal używać twojej lokalnej listy kontaktów, aby znaleźć innych użytkowników Signal.</translation>
    </message>
    <message id="whisperfish-verification-method-label">
        <location filename="../qml/pages/RegisterPage.qml" line="129"/>
        <source>Verification method</source>
        <extracomment>Verification method</extracomment>
        <translation>Metoda weryfikacji</translation>
    </message>
    <message id="whisperfish-use-voice-verification">
        <location filename="../qml/pages/RegisterPage.qml" line="147"/>
        <source>Use voice verification</source>
        <extracomment>Voice verification</extracomment>
        <translation>Użyj weryfikacji głosowej</translation>
    </message>
    <message id="whisperfish-use-text-verification">
        <location filename="../qml/pages/RegisterPage.qml" line="142"/>
        <source>Use text verification</source>
        <extracomment>Text verification</extracomment>
        <translation>Użyj weryfikacji tekstowej</translation>
    </message>
    <message id="whisperfish-registration-title">
        <location filename="../qml/pages/RegisterPage.qml" line="12"/>
        <source>Register</source>
        <extracomment>registration page title</extracomment>
        <translation>Zarejestruj</translation>
    </message>
    <message id="whisperfish-registration-retry-message">
        <location filename="../qml/pages/RegisterPage.qml" line="38"/>
        <source>Please retry with a valid phone number.</source>
        <extracomment>new registration prompt text asking to retry</extracomment>
        <translation>Spróbuj ponownie, podając prawidłowy numer telefonu.</translation>
    </message>
    <message id="whisperfish-registration-phone-number-prefix">
        <location filename="../qml/pages/RegisterPage.qml" line="76"/>
        <source>Prefix</source>
        <extracomment>label for combo box for selecting calling code (phone number prefix) important: translate as short as possible</extracomment>
        <translation>Prefiks</translation>
    </message>
    <message id="whisperfish-registration-number-input-label">
        <location filename="../qml/pages/RegisterPage.qml" line="112"/>
        <source>Phone number</source>
        <extracomment>phone number input label</extracomment>
        <translation>Numer telefonu</translation>
    </message>
    <message id="whisperfish-registration-number-input-placeholder">
        <location filename="../qml/pages/RegisterPage.qml" line="116"/>
        <source>Phone number</source>
        <extracomment>phone number input placeholder</extracomment>
        <translation>Numer telefonu</translation>
    </message>
    <message id="whisperfish-voice-registration-directions">
        <location filename="../qml/pages/RegisterPage.qml" line="134"/>
        <source>Signal will call you with a 6-digit verification code. Please be ready to write it down.</source>
        <oldsource>Signal will call you with a 6-digit verification code. Please be ready to write this down.</oldsource>
        <extracomment>Registration directions</extracomment>
        <translation>Signal zadzwoni do Ciebie z 6-cyfrowym kodem weryfikacyjnym. Przygotuj się, aby to zapisać.</translation>
    </message>
    <message id="whisperfish-text-registration-directions">
        <location filename="../qml/pages/RegisterPage.qml" line="136"/>
        <source>Signal will text you a 6-digit verification code.</source>
        <translation>Signal wyśle tobie 6-cyfrowy kod weryfikacyjny.</translation>
    </message>
    <message id="whisperfish-continue-button-label">
        <location filename="../qml/pages/RegisterPage.qml" line="176"/>
        <location filename="../qml/pages/SetupPasswordPage.qml" line="124"/>
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="114"/>
        <source>Continue</source>
        <extracomment>continue button label</extracomment>
        <translation>Kontynuuj</translation>
    </message>
    <message id="whisperfish-reset-peer-accept">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="24"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="24"/>
        <source>Confirm</source>
        <extracomment>Reset peer identity accept text</extracomment>
        <translation>Potwierdź</translation>
    </message>
    <message id="whisperfish-peer-not-trusted">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="32"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="32"/>
        <source>Peer identity is not trusted</source>
        <extracomment>Peer identity not trusted</extracomment>
        <translation>Tożsamość odbiorcy nie jest zaufana</translation>
    </message>
    <message id="whisperfish-peer-not-trusted-message">
        <location filename="../qml/pages/PeerIdentityChanged.qml" line="42"/>
        <location filename="../qml/pages/ResetPeerIdentity.qml" line="42"/>
        <source>WARNING: %1 identity is no longer trusted. Tap Confirm to reset peer identity.</source>
        <extracomment>Peer identity not trusted message</extracomment>
        <translation>OSTRZEŻENIE: Tożsamość %1 nie jest już zaufana. Dotknij przycisk Potwierdź, aby zresetować tożsamość odbiorcy.</translation>
    </message>
    <message id="whisperfish-settings-linked-devices-menu">
        <location filename="../qml/pages/Settings.qml" line="24"/>
        <source>Linked Devices</source>
        <extracomment>Linked devices menu option</extracomment>
        <translation>Połączone Urządzenia</translation>
    </message>
    <message id="whisperfish-settings-reconnect-menu">
        <location filename="../qml/pages/Settings.qml" line="33"/>
        <source>Reconnect</source>
        <extracomment>Reconnect menu</extracomment>
        <translation>Połącz Ponownie</translation>
    </message>
    <message id="whisperfish-settings-refresh-contacts-menu">
        <source>Refresh Contacts</source>
        <extracomment>Refresh contacts menu</extracomment>
        <translation type="vanished">Odśwież Kontakty</translation>
    </message>
    <message id="whisperfish-settings-title">
        <location filename="../qml/pages/Settings.qml" line="49"/>
        <source>Settings</source>
        <oldsource>Whisperfish Settings</oldsource>
        <extracomment>Settings page title</extracomment>
        <translation>Ustawienia</translation>
    </message>
    <message id="whisperfish-settings-identity-section-label">
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>My Identity</source>
        <extracomment>Settings page My identity section label</extracomment>
        <translation>Moja Tożsamość</translation>
    </message>
    <message id="whisperfish-settings-my-phone-number">
        <location filename="../qml/pages/Settings.qml" line="65"/>
        <source>My Phone</source>
        <oldsource>Phone</oldsource>
        <extracomment>Settings page My phone number</extracomment>
        <translation>MójTelefon</translation>
    </message>
    <message id="whisperfish-settings-my-uuid">
        <location filename="../qml/pages/Settings.qml" line="75"/>
        <source>My UUID registration number</source>
        <extracomment>Settings page My UUID</extracomment>
        <translation>Mój numer rejestracyjny UUID</translation>
    </message>
    <message id="whisperfish-settings-identity-label">
        <location filename="../qml/pages/Settings.qml" line="86"/>
        <source>Identity</source>
        <extracomment>Settings page Identity label</extracomment>
        <translation>Tożsamość</translation>
    </message>
    <message id="whisperfish-settings-notifications-section">
        <location filename="../qml/pages/Settings.qml" line="95"/>
        <source>Notifications</source>
        <extracomment>Settings page notifications section</extracomment>
        <translation>Powiadomienia</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable">
        <location filename="../qml/pages/Settings.qml" line="102"/>
        <source>Enable notifications</source>
        <oldsource>Enabled</oldsource>
        <extracomment>Settings page notifications enable</extracomment>
        <translation>Włącz powiadomienia</translation>
    </message>
    <message id="whisperfish-settings-notifications-enable-description">
        <location filename="../qml/pages/Settings.qml" line="105"/>
        <source>If turned off, Whisperfish will not send any notification</source>
        <extracomment>Settings page notifications enable description</extracomment>
        <translation>Jeśli jest wyłączone, Whisperfish nie wyśle żadnego powiadomienia</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body">
        <location filename="../qml/pages/Settings.qml" line="118"/>
        <source>Show Message Body</source>
        <extracomment>Settings page notifications show message body</extracomment>
        <translation>Pokaż Treść Wiadomości</translation>
    </message>
    <message id="whisperfish-settings-notifications-show-body-description">
        <location filename="../qml/pages/Settings.qml" line="121"/>
        <source>If turned off, Whisperfish will only show the sender of a message, not the contents.</source>
        <extracomment>Settings page notifications show message body description</extracomment>
        <translation>Jeśli jest wyłączone, Whisperfish pokaże tylko nadawcę wiadomości, a nie jej zawartość.</translation>
    </message>
    <message id="whisperfish-settings-general-section">
        <location filename="../qml/pages/Settings.qml" line="136"/>
        <source>General</source>
        <extracomment>Settings page general section</extracomment>
        <translation>Ogólne</translation>
    </message>
    <message id="whisperfish-settings-country-code">
        <location filename="../qml/pages/Settings.qml" line="144"/>
        <source>Country Code</source>
        <extracomment>Settings page country code</extracomment>
        <translation>Kod Kraju</translation>
    </message>
    <message id="whisperfish-settings-country-code-description">
        <location filename="../qml/pages/Settings.qml" line="147"/>
        <source>The selected country code determines what happens when a local phone number is entered.</source>
        <extracomment>Settings page country code description</extracomment>
        <translation>Wybrany numer kierunkowy kraju określa, co się stanie po wprowadzeniu lokalnego numeru telefonu.</translation>
    </message>
    <message id="whisperfish-settings-country-code-empty">
        <location filename="../qml/pages/Settings.qml" line="151"/>
        <source>none</source>
        <extracomment>settings page country code selection: nothing selected</extracomment>
        <translation>żaden</translation>
    </message>
    <message id="whisperfish-settings-save-attachments">
        <location filename="../qml/pages/Settings.qml" line="178"/>
        <source>Save Attachments</source>
        <extracomment>Settings page save attachments</extracomment>
        <translation>Zapisz Załączniki</translation>
    </message>
    <message id="whisperfish-settings-save-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="182"/>
        <source>Attachments are stored at %1. Currently, when disabled, attachments will not work.</source>
        <oldsource>Attachments are stored at %1</oldsource>
        <extracomment>Settings page save attachments description</extracomment>
        <translation type="unfinished">Załączniki są przechowywane w %1</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send">
        <location filename="../qml/pages/Settings.qml" line="215"/>
        <source>Return key send</source>
        <oldsource>EnterKey Send</oldsource>
        <extracomment>Settings page enable enter send</extracomment>
        <translation>Przycisk Enter wysyła</translation>
    </message>
    <message id="whisperfish-settings-enable-enter-send-description">
        <location filename="../qml/pages/Settings.qml" line="218"/>
        <source>When enabled, the return key functions as a send key. Otherwise, the return key can be used for multi-line messages.</source>
        <extracomment>Settings page enable enter send description</extracomment>
        <translation>Po włączeniu klawisz Enter działa jak klawisz wysyłania. W przeciwnym razie w przypadku wiadomości wielowierszowych można użyć klawisza Enter.</translation>
    </message>
    <message id="whisperfish-settings-startup-shutdown-section">
        <location filename="../qml/pages/Settings.qml" line="239"/>
        <source>Autostart and Background</source>
        <extracomment>Settings page startup and shutdown section</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-enable-autostart">
        <location filename="../qml/pages/Settings.qml" line="246"/>
        <source>Autostart after boot</source>
        <extracomment>Settings page enable autostart</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-enable-background-mode-description">
        <location filename="../qml/pages/Settings.qml" line="266"/>
        <source>When enabled, Whisperfish keeps running in the background and can send notifications after the app window has been closed.</source>
        <oldsource>When enabled, Whisperfish starts automatically after each boot. If storage encryption is enabled or background-mode is off, the UI will be shown, otherwise the app starts in the background.</oldsource>
        <extracomment>Settings page enable background mode description</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-enable-background-mode">
        <location filename="../qml/pages/Settings.qml" line="263"/>
        <source>Background mode</source>
        <extracomment>Settings page enable background mode</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-enable-autostart-description">
        <location filename="../qml/pages/Settings.qml" line="249"/>
        <source>When enabled, Whisperfish starts automatically after each boot. If storage encryption is enabled or background-mode is off, the UI will be shown, otherwise the app starts in the background.</source>
        <extracomment>Settings page enable autostart description</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-quit-button">
        <location filename="../qml/pages/Settings.qml" line="283"/>
        <source>Quit Whisperfish</source>
        <extracomment>Settings page quit app button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-settings-advanced-section">
        <location filename="../qml/pages/Settings.qml" line="296"/>
        <source>Advanced</source>
        <extracomment>Settings page advanced section</extracomment>
        <translation>Zaawansowane</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode">
        <location filename="../qml/pages/Settings.qml" line="303"/>
        <source>Incognito Mode</source>
        <extracomment>Settings page incognito mode</extracomment>
        <translation>Tryb Incognito</translation>
    </message>
    <message id="whisperfish-settings-incognito-mode-description">
        <location filename="../qml/pages/Settings.qml" line="306"/>
        <source>Incognito Mode disables storage entirely. No attachments nor messages are saved, messages are visible until restart.</source>
        <extracomment>Settings page incognito mode description</extracomment>
        <translation>Tryb incognito całkowicie wyłącza pamięć. Żadne załączniki ani wiadomości nie są zapisywane, wiadomości są widoczne do ponownego uruchomienia.</translation>
    </message>
    <message id="whisperfish-settings-restarting-message">
        <location filename="../qml/pages/Settings.qml" line="314"/>
        <source>Restart Whisperfish...</source>
        <extracomment>Restart whisperfish message</extracomment>
        <translation>Zrestartuj Whisperfish...</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments">
        <location filename="../qml/pages/Settings.qml" line="327"/>
        <source>Scale JPEG Attachments</source>
        <extracomment>Settings page scale image attachments</extracomment>
        <translation>Skaluj załączniki JPEG</translation>
    </message>
    <message id="whisperfish-settings-scale-image-attachments-description">
        <location filename="../qml/pages/Settings.qml" line="330"/>
        <source>Scale down JPEG attachments to save on bandwidth.</source>
        <extracomment>Settings page scale image attachments description</extracomment>
        <translation>Zmniejsz rozmiar załączników JPEG, aby zaoszczędzić na transferze danych.</translation>
    </message>
    <message id="whisperfish-settings-debug-mode">
        <location filename="../qml/pages/Settings.qml" line="344"/>
        <source>Debug mode</source>
        <extracomment>Settings page: debug info toggle</extracomment>
        <translation>Tryb debuggowania</translation>
    </message>
    <message id="whisperfish-settings-debug-mode-description">
        <location filename="../qml/pages/Settings.qml" line="347"/>
        <source>Show debugging information in the user interface.</source>
        <extracomment>Settings page: debug info toggle extended description</extracomment>
        <translation>Pokaż informacje o debugowaniu w interfejsie użytkownika.</translation>
    </message>
    <message id="whisperfish-settings-stats-section">
        <location filename="../qml/pages/Settings.qml" line="362"/>
        <source>Statistics</source>
        <extracomment>Settings page stats section</extracomment>
        <translation>Statystyki</translation>
    </message>
    <message id="whisperfish-settings-websocket">
        <location filename="../qml/pages/Settings.qml" line="367"/>
        <source>Websocket Status</source>
        <extracomment>Settings page websocket status</extracomment>
        <translation>Status Websocket</translation>
    </message>
    <message id="whisperfish-settings-connected">
        <location filename="../qml/pages/Settings.qml" line="371"/>
        <source>Connected</source>
        <extracomment>Settings page connected message</extracomment>
        <translation>Połączone</translation>
    </message>
    <message id="whisperfish-settings-disconnected">
        <location filename="../qml/pages/Settings.qml" line="374"/>
        <source>Disconnected</source>
        <extracomment>Settings page disconnected message</extracomment>
        <translation>Rozłączone</translation>
    </message>
    <message id="whisperfish-settings-unsent-messages">
        <location filename="../qml/pages/Settings.qml" line="379"/>
        <source>Unsent Messages</source>
        <extracomment>Settings page unsent messages</extracomment>
        <translation>Cofnij Wysłanie Wiadomości</translation>
    </message>
    <message id="whisperfish-settings-total-sessions">
        <location filename="../qml/pages/Settings.qml" line="385"/>
        <source>Total Sessions</source>
        <extracomment>Settings page total sessions</extracomment>
        <translation>Całkowita Ilość Sesji</translation>
    </message>
    <message id="whisperfish-settings-total-messages">
        <location filename="../qml/pages/Settings.qml" line="391"/>
        <source>Total Messages</source>
        <extracomment>Settings page total messages</extracomment>
        <translation>Całkowita Ilość Wiadomości</translation>
    </message>
    <message id="whisperfish-settings-total-contacts">
        <location filename="../qml/pages/Settings.qml" line="397"/>
        <source>Signal Contacts</source>
        <extracomment>Settings page total signal contacts</extracomment>
        <translation>Kontakty Signal</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore">
        <location filename="../qml/pages/Settings.qml" line="403"/>
        <source>Encrypted Key Store</source>
        <extracomment>Settings page encrypted key store</extracomment>
        <translation>Zaszyfrowane Klucze</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-enabled">
        <location filename="../qml/pages/Settings.qml" line="407"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted key store enabled</extracomment>
        <translation>Włączone</translation>
    </message>
    <message id="whisperfish-settings-encrypted-keystore-disabled">
        <location filename="../qml/pages/Settings.qml" line="410"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted key store disabled</extracomment>
        <translation>Wyłączone</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db">
        <location filename="../qml/pages/Settings.qml" line="415"/>
        <source>Encrypted Database</source>
        <extracomment>Settings page encrypted database</extracomment>
        <translation>Zaszyfrowana Baza Danych</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-enabled">
        <location filename="../qml/pages/Settings.qml" line="419"/>
        <source>Enabled</source>
        <extracomment>Settings page encrypted db enabled</extracomment>
        <translation>Włączone</translation>
    </message>
    <message id="whisperfish-settings-encrypted-db-disabled">
        <location filename="../qml/pages/Settings.qml" line="422"/>
        <source>Disabled</source>
        <extracomment>Settings page encrypted db disabled</extracomment>
        <translation>Wyłączone</translation>
    </message>
    <message id="whisperfish-verify-code-accept">
        <source>Verify</source>
        <extracomment>Verify code accept</extracomment>
        <translation type="vanished">Weryfikuj</translation>
    </message>
    <message id="whisperfish-verify-code-title">
        <source>Verify Device</source>
        <extracomment>Verify code page title</extracomment>
        <translation type="vanished">Weryfikuj Urządzenie</translation>
    </message>
    <message id="whisperfish-verify-code-label">
        <source>Code</source>
        <extracomment>Verify code label</extracomment>
        <translation type="vanished">Kod</translation>
    </message>
    <message id="whisperfish-verify-code-placeholder">
        <source>123456</source>
        <oldsource>Code</oldsource>
        <extracomment>Verify code placeholder</extracomment>
        <translation type="obsolete">Kod</translation>
    </message>
    <message id="whisperfish-voice-verify-code-instructions">
        <source>Signal will call you with a 6-digit verification code. Please enter it here.</source>
        <extracomment>Voice verification code instructions</extracomment>
        <translation type="vanished">Signal połączy się z Tobą, podając 6-cyfrowy kod weryfikacyjny. Wprowadź go tutaj.</translation>
    </message>
    <message id="whisperfish-verify-contact-identity-title">
        <location filename="../qml/pages/VerifyIdentity.qml" line="44"/>
        <source>Verify safety numbers</source>
        <oldsource>Verify %1</oldsource>
        <extracomment>Verify safety numbers</extracomment>
        <translation>Weryfikuj numery bezpieczeństwa</translation>
    </message>
    <message id="whisperfish-group-add-member-menu">
        <location filename="../qml/pages/GroupProfilePage.qml" line="200"/>
        <source>Add Member</source>
        <extracomment>Add group member menu item</extracomment>
        <translation>Dodaj Członka</translation>
    </message>
    <message id="whisperfish-group-add-member-remorse">
        <source>Adding %1 to group</source>
        <oldsource>%1 added to group</oldsource>
        <extracomment>Add group member remorse message</extracomment>
        <translation type="vanished">%1 dodany do grupy</translation>
    </message>
    <message id="whisperfish-unknown-contact">
        <location filename="../qml/pages/GroupProfilePage.qml" line="46"/>
        <source>Unknown</source>
        <extracomment>Unknown contact in group member list</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-leave-menu">
        <location filename="../qml/pages/GroupProfilePage.qml" line="172"/>
        <source>Leave this group</source>
        <oldsource>Leave</oldsource>
        <extracomment>Leave group menu item</extracomment>
        <translation type="unfinished">Opuść</translation>
    </message>
    <message id="whisperfish-group-leave-remorse">
        <location filename="../qml/pages/GroupProfilePage.qml" line="180"/>
        <source>Leaving group and removing ALL messages!</source>
        <oldsource>Leaving group. This will permanently remove ALL group messages!</oldsource>
        <extracomment>Leave group remorse message</extracomment>
        <translation>Opuszczasz grupę. Spowoduje to usunięcie na stałe WSZYSTKICH wiadomości grupowych!</translation>
    </message>
    <message id="whisperfish-group-invite-link-menu">
        <location filename="../qml/pages/GroupProfilePage.qml" line="193"/>
        <source>Create invitation link</source>
        <extracomment>Create invite link menu item</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-member-menu-direct-message">
        <location filename="../qml/pages/GroupProfilePage.qml" line="227"/>
        <source>Message to %1</source>
        <extracomment>Menu item to start a private chat with a group member</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-member-menu-save-contact">
        <location filename="../qml/pages/GroupProfilePage.qml" line="236"/>
        <source>Add to contacts</source>
        <extracomment>Menu item to save a group member to the local address book</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-member-menu-verify-fingerprint">
        <location filename="../qml/pages/GroupProfilePage.qml" line="243"/>
        <source>Verify safety number</source>
        <extracomment>Menu item to verify safety numbers with a group member</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-member-menu-remove-from-group">
        <location filename="../qml/pages/GroupProfilePage.qml" line="257"/>
        <source>Remove from this group</source>
        <extracomment>Menu item to remove a member from a group (requires admin privileges)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-member-name-self">
        <location filename="../qml/pages/GroupProfilePage.qml" line="292"/>
        <source>You</source>
        <extracomment>Title for the user&apos;s entry in a list of group members</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-group-members-title">
        <source>Group members</source>
        <extracomment>Group members</extracomment>
        <translation type="vanished">Członkowie grupy</translation>
    </message>
    <message id="whisperfish-info-page-default-title">
        <location filename="../qml/components/BlockingInfoPageBase.qml" line="17"/>
        <source>Whisperfish</source>
        <extracomment>default title of full-screen info pages (below the icon)</extracomment>
        <translation>Whisperfish</translation>
    </message>
    <message id="whisperfish-fatal-error-title">
        <location filename="../qml/pages/FatalErrorPage.qml" line="11"/>
        <source>Error</source>
        <extracomment>fatal error page title</extracomment>
        <translation>Błąd</translation>
    </message>
    <message id="whisperfish-fatal-error-hint">
        <location filename="../qml/pages/FatalErrorPage.qml" line="18"/>
        <source>Please restart Whisperfish. If the problem persists and appears to be an issue with Whisperfish, please report the issue.</source>
        <extracomment>generic hint on what to do after a fatal error occurred (error message will be shown separately)</extracomment>
        <translation>Uruchom ponownie Whisperfish. Jeśli problem nie ustąpi i wydaje się, że dotyczy Whisperfish, zgłoś go.</translation>
    </message>
    <message id="whisperfish-startup-placeholder-title">
        <location filename="../qml/pages/LandingPage.qml" line="73"/>
        <source>Welcome</source>
        <extracomment>welcome text shown when startup takes a long time</extracomment>
        <translation>Witamy</translation>
    </message>
    <message id="whisperfish-verify-page-title">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="10"/>
        <source>Verify</source>
        <extracomment>verify registration page title</extracomment>
        <translation>Weryfikuj</translation>
    </message>
    <message id="whisperfish-verify-code-prompt">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="13"/>
        <source>Please enter the code you received from Signal.</source>
        <extracomment>verify registration prompt</extracomment>
        <translation>Wprowadź kod otrzymany od Signal.</translation>
    </message>
    <message id="whisperfish-verify-instructions-voice">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="20"/>
        <source>Signal should have called you with a a 6-digit verification code. Please wait a moment, or restart the process if you have not received a call.</source>
        <extracomment>verify registration instructions: voice</extracomment>
        <translation>Signal powinien był zadzwonić do Ciebie z 6-cyfrowym kodem weryfikacyjnym. Poczekaj chwilę lub zrestartuj proces, jeśli nie otrzymałeś połączenia.</translation>
    </message>
    <message id="whisperfish-verify-instructions-sms">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="25"/>
        <source>Signal should have sent you a 6-digit verification code via text message. Please wait a moment, or restart the process if you have not received a message.</source>
        <extracomment>verify registration instructions: text message</extracomment>
        <translation>Signal powinien wysłać Ci 6-cyfrowy kod weryfikacyjny w wiadomości tekstowej. Poczekaj chwilę lub ponownie uruchom proces, jeśli nie otrzymałeś wiadomości.</translation>
    </message>
    <message id="whisperfish-verify-retry-prompt">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="43"/>
        <source>Please retry with a valid code.</source>
        <extracomment>verification: prompt to retry with a new code</extracomment>
        <translation>Spróbuj ponownie, używając prawidłowego kodu.</translation>
    </message>
    <message id="whisperfish-verify-code-input-label">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="84"/>
        <source>Verification code</source>
        <extracomment>verification code input label</extracomment>
        <translation>Kod weryfikacyjny</translation>
    </message>
    <message id="whisperfish-verify-code-input-placeholder">
        <location filename="../qml/pages/VerifyRegistrationPage.qml" line="87"/>
        <source>Code</source>
        <extracomment>verification code input placeholder</extracomment>
        <translation>Kod</translation>
    </message>
    <message id="whisperfish-attachment-preview-contact-title">
        <location filename="../qml/components/attachment/AttachmentItemContact.qml" line="22"/>
        <source>Shared contact</source>
        <extracomment>Placeholder shown as title for an attached contact in a message</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-chat-input-attachment-label" numerus="yes">
        <location filename="../qml/components/ChatTextInput.qml" line="149"/>
        <source>%n attachment(s)</source>
        <extracomment>Number of attachments currently selected for sending</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message id="whisperfish-chat-input-placeholder-personal">
        <location filename="../qml/components/ChatTextInput.qml" line="157"/>
        <source>Hi %1</source>
        <extracomment>Personalized placeholder for chat input, e.g. &quot;Hi John&quot;</extracomment>
        <translation type="unfinished">Cześć, %1</translation>
    </message>
    <message id="whisperfish-chat-input-placeholder-default">
        <location filename="../qml/components/ChatTextInput.qml" line="161"/>
        <source>Write a message</source>
        <extracomment>Generic placeholder for chat input</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-select-attachments-page-title">
        <location filename="../qml/components/ChatTextInput.qml" line="211"/>
        <source>Select attachments</source>
        <extracomment>Attachment picker page title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-attachments-loader-show-more" numerus="yes">
        <location filename="../qml/components/message/AttachmentsLoader.qml" line="278"/>
        <source>and %1 more</source>
        <extracomment>Note if some message attachments are hidden instead of being shown inline</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message id="whisperfish-message-no-timestamp">
        <location filename="../qml/components/message/InfoRow.qml" line="46"/>
        <source>no time</source>
        <extracomment>Placeholder note if a message doesn&apos;t have a timestamp (which must not happen).</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-message-show-less">
        <location filename="../qml/components/message/InfoRow.qml" line="83"/>
        <source>show less</source>
        <extracomment>Hint for very long messages, while expanded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-message-show-more">
        <location filename="../qml/components/message/InfoRow.qml" line="86"/>
        <source>show more</source>
        <extracomment>Hint for very long messages, while not expanded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-sender-name-label-outgoing">
        <location filename="../qml/components/QuotedMessagePreview.qml" line="98"/>
        <location filename="../qml/components/SenderNameLabel.qml" line="81"/>
        <source>You</source>
        <extracomment>Name shown when replying to own messages</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-quoted-message-preview-attachment">
        <location filename="../qml/components/QuotedMessagePreview.qml" line="121"/>
        <source>Attachment</source>
        <extracomment>Placeholder text if quoted message preview contains no text, only attachments</extracomment>
        <translation type="unfinished">Załącznik</translation>
    </message>
    <message id="whisperfish-sender-label-empty">
        <location filename="../qml/delegates/MessageDelegate.qml" line="169"/>
        <source>no sender</source>
        <extracomment>Label shown if a message doesn&apos;t have a sender.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-message-empty-note">
        <location filename="../qml/delegates/MessageDelegate.qml" line="220"/>
        <source>this message is empty</source>
        <extracomment>Placeholder note if an empty message is encountered.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-joined-group-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="49"/>
        <source>You joined the group “%1”.</source>
        <extracomment>Service message, %1 = group name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-left-group-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="53"/>
        <source>You left the group.</source>
        <extracomment>Service message</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-joined-group-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="57"/>
        <source>%1 joined the group.</source>
        <extracomment>Service message, %1 is the new member&apos;s name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-left-group-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="61"/>
        <source>%1 left the group.</source>
        <extracomment>Service message, %1 is is the lost member&apos;s name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-missed-call-voice">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="65"/>
        <source>You missed a call from %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-missed-call-video">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="69"/>
        <source>You missed a video call from %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-call-voice-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="73"/>
        <source>You called %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-call-video-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="77"/>
        <source>You started a video call with %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-call-voice-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="81"/>
        <source>%1 called you.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-call-video-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="85"/>
        <source>%1 started a video call with you.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-fingerprint-changed">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="90"/>
        <source>Your safety number with %1 has changed. Swipe right to verify the new number.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-session-reset-self">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="94"/>
        <source>You have reset the secure session with %1.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-session-reset-peer">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="98"/>
        <source>%1 has reset the secure session with you.</source>
        <extracomment>Service message, %1 is a name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-not-supported">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="103"/>
        <source>This service message is not yet supported by Whisperfish. Please file a bug report. (Type: &apos;%1&apos;.)</source>
        <extracomment>Service message, %1 is an internal message type identifier</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-service-message-more-info">
        <location filename="../qml/delegates/ServiceMessageDelegate.qml" line="160"/>
        <source>more information</source>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-contact-card-page-save">
        <location filename="../qml/pages/ContactCardPage.qml" line="53"/>
        <source>Save to address book</source>
        <extracomment>Menu item to save a shared contact to the local address book</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-expanded-message-page-header">
        <location filename="../qml/pages/ExpandedMessagePage.qml" line="30"/>
        <source>Full message</source>
        <extracomment>Page title for a very long message shown on a page of its own</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-expanded-message-info-outbound">
        <location filename="../qml/pages/ExpandedMessagePage.qml" line="34"/>
        <source>to %1</source>
        <extracomment>Page description for a very long message shown on a page of its own</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-expanded-message-info-inbound">
        <location filename="../qml/pages/ExpandedMessagePage.qml" line="37"/>
        <source>from %1</source>
        <extracomment>Page description for a very long message shown on a page of its own</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-view-image-page-loading">
        <location filename="../qml/pages/ViewImagePage.qml" line="242"/>
        <source>Loading image</source>
        <extracomment>Full page placeholder shown while a large image is being loaded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-view-image-page-error">
        <location filename="../qml/pages/ViewImagePage.qml" line="252"/>
        <source>Failed to load</source>
        <extracomment>Full page placeholder shown when an image failed to load</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="whisperfish-view-video-page-error">
        <location filename="../qml/pages/ViewVideoPage.qml" line="105"/>
        <source>Failed to play</source>
        <extracomment>Full page placeholder shown when a video failed to load</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
